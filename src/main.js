import Vue from 'vue'
import App from './App.vue'
import VueScrollReveal from 'vue-scroll-reveal'
import Buefy from 'buefy'
import router from './router'

Vue.config.productionTip = false

Vue.use(Buefy)
Vue.use(VueScrollReveal, {
  class: 'v-scroll-reveal',
  duration: 800,
  scale: 1,
  distance: '10px',
  mobile: false
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
